# Docker Basic Commands

## Image Related Commands
- List existing images
```bash
docker images
```
- Pull Image
```bash
docker pull <image_name>:<tag> 
# docker pull nginx
```
```bash
docker rmi <image_name>
# delete the image
```
```bash
docker tag <existing_image_name>:<tag> <new_image_name>:<tag>
docker push <full_repo_name>:<tag>
```
## Container related commands

```bash
docker run <image_name>:<tag>
# Create a new container using the image
```

```bash
docker stop <container_name>
# Stops the container
```

```bash
docker start <container_name_or_id>
# starts a stopped container
```

```bash
docker rm <container_name_or_id>
# remove one container
```

```bash
docker run --name my-site -p 8080:80 -v /home/nobleprog/Desktop/docker-k8s/web:/usr/share/nginx/html -d nginx
# Creating a container with mapped port and shared directory
```

```bash
docker commit <container_name> <image_name>
## Docker related commands

```bash
docker ps
# list running containers
docker ps -q
# list running container ids only
docker ps -a
# list all containers
docker ps -aq
# list all running containers by id only
```

```bash
docker images
#list all the images